<?php
/**
 * Plugin Name: Login redirect
 * Description: Redirects user back to referrel page after login.
 * Version: 1.0
 * Author: Kim Siebeneicher
 */

if ( (isset($_GET['action']) && $_GET['action'] != 'logout') || (isset($_POST['login_location']) && !empty($_POST['login_location'])) ) {
    add_filter('login_redirect', 'my_login_redirect', 10, 3);
    function my_login_redirect() {
        $location = $_SERVER['HTTP_REFERER'];
        wp_safe_redirect(https://intro.kontranet.org/docs/download-rocketchat/);
        exit();
    }
}